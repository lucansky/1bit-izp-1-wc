/*
 * Subor:   proj1.c
 * Datum:   2013/11/06
 * Autor:   Adam Lucansky <xlucan01@stud.fit.vutbr.cz>
 * Projekt: Pocitanie slov, projekt c. 1 pre predmet IZP
 * Popis:   Program pocita pocet slov v ktorych je aspon raz nalez daneho znaku 
 *          zo vstupu stdin
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>

/** Maximum length of word that is printed in debug mode */
#define MAX_DEBUG_WORD 80

/** Macro for checking if character is delimiter, 
  * calling function is ineffecient
  */
#define isdelimiter(ch) (!((ch >= 'a' && ch <= 'z') \
                        || (ch >= 'A' && ch <= 'Z') \
                        || (ch >= '0' && ch <= '9') \
                        || (ch == '-') || (ch == '_')))

/** Help message */
const char *HELPMSG =
  "Usage: ./proj1 --help\n"
  "  or:  ./proj1 LETTER [POSITION] [OPTIONS]\n"
  "Prints occurrences of given letter in every word (only once) \n"
  "from directed INPUT(stdin)\n"
  "Author: Adam Lucansky (c) 2013 \n"
  "\n"
  "  LETTER\t letter to seek for from input\n"
  "  [POSITION]\t exact position of letter to look for\n"
  "  -d\t\t debug mode (prints words, only up to 80 chars)\n"
  "  Accepted wildcards in letter:\n"
  "  : - any number\n"
  "  ^ - any uppercase letter\n"
  "  . - any letter\n"
  "Report bugs to <xlucan01@stud.fit.vutbr.cz>\n";

/** Error codes */
enum tecodes
{
      EOK = 0,  ECLWRONG,           EUNKNOWN
  //   OK       Incorrect           Unknown
  //            params              error
};

/** Translations */
const char *ECODEMSG[] =
{
  [EOK] = "OK\n",
  [ECLWRONG] = "Incorrect parameters\n",
  [EUNKNOWN] = "Unknown error\n"
};

/** Program state codes */
enum tstates
{
  SOK,           /**< Regular program run */
  SHELP         /**< Show help */
};

/**
 * Structure defining parameters
 */
typedef struct params
{
  bool letter_set;         /**< Indicating whether letter is set or not */
  unsigned int letter;     /**< Desired letter from args */
  
  bool position_set;     /**< Indicating whether position is set or not*/
  unsigned long position; /**< Exact position of letter (optional) */
  
  unsigned int letter_up_to; /**< Uppermost letter from range, discussed later*/
  
  bool debug;        /**< Debug mode */
  
  int ecode;         /**< Error code according to enum tecodes. */
  int state;         /**< State code according to enum tstates. */
} TParams;

/**
 * Prints error message corresponding 
 * @param ecode error code
 */
void printECode(int ecode)
{
  if (ecode < EOK || ecode > EUNKNOWN)
  { ecode = EUNKNOWN; }

  fprintf(stderr, "%s", ECODEMSG[ecode]);
}

/**
 * Compares arguments for string equality
 * @param str1 First string
 * @param str2 Second string
 * @return Returns whether both arguments are equal
 */
bool equals(const char *str1, const char *str2) {
  return strcmp(str1, str2) == 0;
}

/**
  * Checking whether argument consist of only 0-9
  * @param str String
  * @return Returns true if str =~ /^[0-9]*$/, else false
  */
bool isnum(const char *str) {
  if(strlen(str) == 0) return false;
  
  unsigned int i = 0;
  
  while(str[i] != '\0') {
    if(str[i] < '0' || str[i] > '9') return false;
    i++;
  }
  return true;
}

/**
  * Converts string to unsigned long, 
  * consistency of input must be validated with isnum()
  * Returns 0 if unsigned long is about to overflow during iteration
  * @param str String
  * @returns Returns decimal value of ASCII string containing numbers
  */
unsigned long str_to_ul(const char *str) {
  if(strlen(str) == 0) return 0;
  
  unsigned long result = 0;
  unsigned int i = 0;
  while(str[i] != '\0') {
    // Overflow validation
    if(ULONG_MAX / 10 < result) return 0;
    result *= 10;
    result += str[i]-'0';
    i++;
  }
  return result;
}

/**
 * Processes arguments and returns TParams structure containing necessary fields
 * If format of arguments is incorrect, returns error state in field state.
 * @param argc Arguments length.
 * @param argv Array of strings from arguments.
 * @return Returns analysed arguments from arguments.
 */
TParams getParams(int argc, char *argv[])
{
  TParams result =
  {
    /** Default values */
    .letter_set = false,
    .letter = '\0',
    .letter_up_to = '\0',
    
    .position_set = false,
    .position = 0,
    .ecode = EOK,
    .state = SOK
  };
  
  int i;
  // Iterating through arguments
  for(i = 1; i < argc; i++)
  {
    // First mandatory argument
    if(i == 1)
    {
      if(equals(argv[i], "--help")) result.state = SHELP;
      // Must be exactly 1 char/byte long
      if(strlen(argv[i]) == 1)
      {
        /** Heuristic done by pre-setting range of wildcard, 
          * therefore not needing to compare it with every char
          */
        switch(argv[i][0]) {
          // Number wildcard
          case ':': 
            result.letter = '0';
            result.letter_up_to = '9';
            break;

          // Uppercase wildcard
          case '^':
            result.letter = 'A';
            result.letter_up_to = 'Z';
            break;

          // Any byte value matches
          case '.':
            result.letter = '\0';
            result.letter_up_to = '\255';
            break;

          // Not wildcard
          default:
            result.letter = argv[i][0];
        }
        // Assuming if it gets here, letter is set
        result.letter_set = true;
      } 
      
    }
    // Optional arguments
    if(i > 1) {
      if(!result.position_set && isnum(argv[i])) {
        result.position = str_to_ul(argv[i]);
        result.position_set = true;
      }
      if(equals(argv[i], "-d") || equals(argv[i], "--debug")) {
        result.debug = true;
      }
    }
  }
  // This indicates mandatory parameter not parsed
  if(!result.letter_set) result.ecode = ECLWRONG;

  return result;
}

/**
 * Depending on wildcards, returns whether current character is 
 * matching with desired. If letter_up_to set, comparing with range (wildcard).
 * @param ch Current character.
 * @param params Pointer to parameters parsed from arguments.
 * @return Returns true if character matches the one user is looking for.
 */
bool isdesired(unsigned int ch, TParams *params) {
  if(params->letter_up_to > params->letter) {
    return (params->letter <= ch && ch <= params->letter_up_to);
  } else {
    return params->letter == ch;
  }
}

/**
 * Core function returning count of words with given letter from arguments.
 * Input is from stdin.
 * @param params Pointer to parameters parsed from arguments.
 * @return Returns occurrences found from input.
 */
unsigned long occurences_from_stdin(TParams *params) {
  int current_char;
  unsigned long current_pos = 0;
  // Number of words with desired letter (or wildcards)
  unsigned long occurrences = 0; 
  // Default true to ensure ignoring first whitespace
  bool at_beginning = true; 
  // Default false, indicating whether in whitespace or not
  bool in_space = false;  
  bool already_incremented = false;
  
  while ((current_char = getchar()) != EOF)
  {
    if (isdelimiter(current_char))
    {
      if (!at_beginning && !in_space) putchar('\n');
    
      // Whitespace indicates new word, therefor resets required
      already_incremented = false; 
      current_pos = 0;
      in_space = true;
    } else {
      at_beginning = false;
      in_space = false;
      current_pos++;
      
      if(!already_incremented && isdesired(current_char, params) &&
          (!params->position_set || (params->position_set 
                                    && current_pos == params->position))) 
      {
        occurrences++;
        already_incremented = true;
      }
      if(params->debug && current_pos <= MAX_DEBUG_WORD) putchar(current_char);
    }
  }
  return occurrences;
}

/**
 * Main function firing up arguments parsing and core functions
 * @param argc Arguments count.
 * @param argv Array of strings.
 * @return Returns status code depending on user interaction.
 */
int main(int argc, char *argv[])
{
  TParams params = getParams(argc, argv);
  if (params.ecode != EOK)
  {
    printECode(params.ecode);
    printf("%s", HELPMSG);
    return EXIT_FAILURE;
  }

  if (params.state == SHELP)
  {
    printf("%s", HELPMSG);
    return EXIT_SUCCESS;
  }

  // Core of program, always passing pointer without duplication
  printf("%lu\n", occurences_from_stdin(&params));

  return EXIT_SUCCESS;
}

/* EOF proj1.c */
